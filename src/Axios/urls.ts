export const loginUTR     = 'https://school-app-77.herokuapp.com/api/login';


export const getAdminsURL = "https://school-app-77.herokuapp.com/api/admin/get-admins";
export const createAdminsURL = "https://school-app-77.herokuapp.com/api/admin/create-admin";
export const editAdminsURL = (id:string) => `https://school-app-77.herokuapp.com/api/admin/update-admin/${id}`;
export const deleteAdminsURL = (id:string) => `https://school-app-77.herokuapp.com/api/admin/delete-admin/${id}`;


export const getTeachersURL = "https://school-app-77.herokuapp.com/api/teacher/get-teachers";
export const createTeacherURL = "https://school-app-77.herokuapp.com/api/teacher/create-teacher";
export const editTeacherURL = (id:string) => `https://school-app-77.herokuapp.com/api/teacher/update-teacher/${id}`;
export const deleteTeacherURL = (id:string) => `https://school-app-77.herokuapp.com/api/teacher/delete-teacher/${id}`;

export const getGradesURL = "https://school-app-77.herokuapp.com/api/grade/get-grades";


