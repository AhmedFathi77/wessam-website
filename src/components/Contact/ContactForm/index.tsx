import React, { useState, useEffect } from 'react';
import * as styles from './styles.module.css';


const ContactForm: React.FC= (props) =>{
    
    return (
        <div className={`${styles.default.wrapper}`}>
			
			<form action="" className={styles.default.contact_form}>
            <div className="section-title text-center mb-5">
                <span className="sub-title mb-2 d-block">Get In Touch</span>
                <h2 className="title text-primary">Contact Us</h2>
            </div>

				<div className="row mb-4">
				<div className="col-md-6 mb-4 mb-md-0">
					<input type="text" className="form-control" style={{height: "3rem"}} placeholder="First name" />
				</div>
				<div className="col-md-6">
					<input type="text" className="form-control" style={{height: "3rem"}} placeholder="Last name" />
				</div>
				</div>

				<div className="row mb-4">
				<div className="col-12">
					<input type="text" className="form-control" style={{height: "3rem"}} placeholder="Email" />
				</div>
				</div>

				<div className="row mb-4">
				<div className="col-12">
					<textarea className="form-control" rows={5} cols={3} name="" id="" placeholder="Message"></textarea>
				</div>
				</div>

				<div className="row">
				<div className="col-12">
					<button type="submit" className="btn btn-primary btn-md">Send Message</button>
				</div>
				</div>

				</form>
		</div>


    );
}

export default ContactForm;