import React, { useState, useEffect } from 'react';
import * as styles from './styles.module.css';
import SingleBlogCard from '../SingleBlog';


const HomeBlog: React.FC = (props) =>{
    
    return (
        <div className={`${styles.default.wrapper}`}>
            <div className={styles.default.headerWrapper}>
                <p className={styles.default.header}>Blog Posts</p>
            </div>
            <div className={styles.default.blogWrapper}>
                    <SingleBlogCard />
        
                    <SingleBlogCard />
            
                    <SingleBlogCard />
                
                    <SingleBlogCard />   
            
            </div>
        
        </div>

    );
}

export default HomeBlog;