import React, { useState, useEffect } from 'react';
import * as styles from './styles.module.css';
import NavbarComponent from '../navbar';
import HomeAbout from './HomeAbout';
import CustomCarousel from './Carousel';
import HomeGallery from './HomeGallery';
import HomeBlog from './Blog/HomeBlog';
import Footer from '../Footer';
import Testimonial from '../Home/Testimonial';


const HomePage: React.FC = (props) =>{
    const [isActive, setIsActive] = useState(false);
    const [topPage, setTopPage] = useState<{}>();
    const [wrapRef, setWrapRef] = useState<HTMLDivElement>();

    useEffect(()=>{
        window.addEventListener('scroll', handleScroll);
        
    },[topPage]);
    const handleScroll = () => {
        const { top } = (wrapRef === undefined)?{top:-999}:wrapRef.getBoundingClientRect();
        setTopPage((top - top - top));
        if ((top - top - top) > 20) {
            setIsActive(true);
        }
        if ((top - top - top) <= 20) {
            setIsActive(false);
        }
    }
    
    const setWrapRefHandler = (ref:any) => {
        setWrapRef(ref)
    }

    
    return (
        <div className={`${styles.default.wrapper}`}>
            <div ref={setWrapRefHandler}>
                <NavbarComponent active={isActive} />
                <CustomCarousel  />
                <HomeAbout />
                <HomeGallery />
                <HomeBlog />
                <Testimonial />
                <Footer />
            </div>
        </div>
    );
}

export default HomePage;