import React from 'react';
import * as styles from './styles.module.css';


const SingleBlogCard: React.FC = (props) =>{
    
    return (
            <div className={`${styles.default.blog_card} ${styles.default.blog_card_blog}`}>
                <div className={`${styles.default.blog_card_image}`}>
                    <a > 
                        <img className={styles.default.img} src="https://picsum.photos/id/1084/536/354?grayscale" /> 
                    </a>
                    <div className={styles.default.ripple_cont}></div>
                </div>
                <div className={styles.default.blog_table}>
                    <h6 className={`${styles.default.blog_category} ${styles.default.blog_text_success}`}>
                        <i className={`${styles.default.far} ${styles.default.fa_newspaper}`}></i></h6>
                    <h4 className={styles.default.blog_card_caption}>
                        <a >Lorem Ipsum is simply dummy text of the printing and</a>
                    </h4>
                    <p className={styles.default.blog_card_description}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <div className={styles.default.ftr}>
                        <div className={styles.default.author}>
                            <a> <img src="https://picsum.photos/id/1005/5760/3840" alt="..." className={`${styles.default.avatar} ${styles.default.img_raised}`}  /> <span>Lorem</span> </a>
                        </div>
                        <div className={styles.default.stats}> <i className={`${styles.default.far} ${styles.default.fa_clock}`}></i> 10 min </div>
                    </div>
                </div>
            </div>

    );
}

export default SingleBlogCard;