import React, { useState } from 'react';
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import * as styles from './styles.module.sass';
const items = [
{
    src: 'https://www.vitalimages.com/wp-content/uploads/SIIM2020_Homepage_Banner.jpg',
    altText: 'Slide 1',
    caption: 'Slide 1'
},
{
    src: 'https://cdn.suwalls.com/wallpapers/abstract/connected-dots-23452-2880x1800.jpg',
    altText: 'Slide 2',
    caption: 'Slide 2'
},
{
    src: 'https://www.tripsavvy.com/thmb/4Swfscl2UAWR1B_8WyE_LH14pKA=/2120x1414/filters:fill%28auto,1%29/GettyImages-938174878-534efd6cc28e45e78fae6a841c1e0aaf.jpg',
    altText: 'Slide 3',
    caption: 'Slide 3'
}
];

const CustomCarousel = (props:any) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex:any) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const slides = items.map((item) => {
        return (
        <CarouselItem
            onExiting={() => setAnimating(true)}
            onExited={() => setAnimating(false)}
            key={item.src}
            className={styles.default.sliderImageWrapper}
        >
            <img src={item.src} alt={item.altText} />
            <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
        </CarouselItem>
        );
    });

    return (
        <Carousel
            activeIndex={activeIndex}
            next={next}
            previous={previous}
        >
            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
                {slides}
            <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
            <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
        </Carousel>
    );
}

export default CustomCarousel;