import React, { useState, useEffect } from 'react';
import * as styles from './styles.module.css';


const HomeAbout: React.FC = (props) =>{
    
    return (
        <div className={`${styles.default.wrapper}`}>
            <div className={styles.default.leftImage}>
                <img src={require('../../../assets/glint_hero_2.jpg')} />
            </div>
            <div className={styles.default.contentWrapper}>
                <p className={styles.default.header}>About Me</p>
                <p className={styles.default.about}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde modi blanditiis iure.
                </p>
                <button className={styles.default.courses_button}>View Courses</button>
            </div>
        </div>

    );
}

export default HomeAbout;