import React from 'react';
import * as styles from './styles.module.css';

interface IProps{
    name: string;
    // img: string;
    // role: string;
    testimonial:string;
}

const SingleTestimonialCard: React.FC<IProps> = (props) =>{
    
    return (
        <div className="testimonial4_slide">
            <img src="https://i.ibb.co/8x9xK4H/team.jpg" className={`${styles.default.img_circle} ${styles.default.img_responsive}`} />
            <p>{props.testimonial}</p>
            <h4>{props.name}</h4>
        </div>
    );
}

export default SingleTestimonialCard;